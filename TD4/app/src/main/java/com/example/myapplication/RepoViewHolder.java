package com.example.myapplication;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RepoViewHolder extends RecyclerView.ViewHolder {

    private TextView nom;

    public RepoViewHolder(@NonNull View itemView) {
        super(itemView);

        nom = itemView.findViewById(R.id.Nom);
    }

    public void display(Repo repo)
    {
        nom.setText(repo.getName());
    }
}
