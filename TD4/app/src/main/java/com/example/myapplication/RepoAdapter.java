package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.zip.Inflater;

public class RepoAdapter extends RecyclerView.Adapter<RepoViewHolder> {

    List<Repo> repos;

    public RepoAdapter(List<Repo> newRepos)
    {
        this.repos = newRepos;
    }

    @NonNull
    @Override
    public RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo,parent,false);
        return new RepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoViewHolder holder, int position) {
        holder.display(repos.get(position));
    }

    @Override
    public int getItemCount() {
        return repos.size();
    }
}
