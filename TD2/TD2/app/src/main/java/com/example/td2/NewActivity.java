package com.example.td2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NewActivity extends AppCompatActivity implements View.OnClickListener {

    private Button detailsButton;
    private Button logOutButton;
    private Button aboutButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        this.detailsButton = findViewById(R.id.detailsButton);
        this.logOutButton = findViewById(R.id.logoutButton);
        this.aboutButton = findViewById(R.id.aboutButton);

        this.detailsButton.setOnClickListener(this);
        this.logOutButton.setOnClickListener(this);
        this.aboutButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;

        switch (v.getId())
        {
            case R.id.detailsButton :
                intent = new Intent(this, DetailsActivity.class);
                startActivity(intent);
                break;
            case R.id.logoutButton :
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.aboutButton :
                String url = "http://android.busin.fr/";
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                break

        }
    }

    @Override
    public void onBackPressed()
    {
        //moveTaskToBack(true);
        super.onBackPressed();
        finish();
    }
}
