package com.example.td3;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyVideosGamesViewHolder extends RecyclerView.ViewHolder {

    private TextView mNom;
    private TextView mPrice;

    public MyVideosGamesViewHolder(@NonNull View itemView) {
        super(itemView);

        mNom = itemView.findViewById(R.id.name);
        mPrice = itemView.findViewById(R.id.price);
    }

    public void display(JeuVideo jeu)
    {
        mNom.setText(jeu.getNom());
        mPrice.setText(jeu.getPrix()+"$");
    }
}
