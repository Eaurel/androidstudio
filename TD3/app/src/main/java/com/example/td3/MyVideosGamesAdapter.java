package com.example.td3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyVideosGamesAdapter extends RecyclerView.Adapter<MyVideosGamesViewHolder> {

    private List<JeuVideo> listJeux;

    public MyVideosGamesAdapter(List<JeuVideo> jeux)
    {
        listJeux = jeux;
    }

    @NonNull
    @Override
    public MyVideosGamesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jeu_video,parent,false);
        return new MyVideosGamesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyVideosGamesViewHolder holder, int position) {
        holder.display(listJeux.get(position));
    }

    @Override
    public int getItemCount() {
        return listJeux.size();
    }
}
