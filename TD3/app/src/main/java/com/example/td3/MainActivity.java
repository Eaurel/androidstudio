package com.example.td3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<JeuVideo> mesJeux = new ArrayList<JeuVideo>();
        mesJeux.add(new JeuVideo("Pokémon Soleil",25));
        mesJeux.add(new JeuVideo("Pokémon épée",45));
        mesJeux.add(new JeuVideo("Pokemon Y",10));
        mesJeux.add(new JeuVideo("Pokémon Ultra Soleil",30));

        recycler = findViewById(R.id.myRecyclerView);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(new MyVideosGamesAdapter(mesJeux));
    }
}
