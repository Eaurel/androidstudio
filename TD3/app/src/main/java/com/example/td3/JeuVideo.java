package com.example.td3;

public class JeuVideo {

    private String nom;
    private int prix;

    public JeuVideo(String name, int prix)
    {
        this.nom = name;
        this.prix = prix;
    }

    public String getNom()
    {
        return nom;
    }

    public int getPrix()
    {
        return prix;
    }

    public void setNom(String nNom)
    {
        this.nom = nNom;
    }

    public void setPrix(int nPrix)
    {
        this.prix = nPrix;
    }

}
