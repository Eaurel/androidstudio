package com.example.calculatrice;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText affichage;
    private String calcul;
    private int[] nombres;
    private int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        affichage = findViewById(R.id.editText);
        calcul = "";
        nombres = new int[200];
        i=0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void bouton1(View v)
    {
        calcul = calcul + "1";
        affichage.setText(calcul);
    }

    public void bouton2(View v)
    {
        calcul = calcul + "2";
        affichage.setText(calcul);
    }

    public void bouton3(View v)
    {
        calcul = calcul + "3";
        affichage.setText(calcul);
    }

    public void bouton4(View v)
    {
        calcul = calcul + "4";
        affichage.setText(calcul);
    }

    public void bouton5(View v)
    {
        calcul = calcul + "5";
        affichage.setText(calcul);
    }

    public void bouton6(View v)
    {
        calcul = calcul + "6";
        affichage.setText(calcul);
    }

    public void bouton7(View v)
    {
        calcul = calcul + "7";
        affichage.setText(calcul);
    }

    public void bouton8(View v)
    {
        calcul = calcul + "8";
        affichage.setText(calcul);
    }

    public void bouton9(View v)
    {
        calcul = calcul + "9";
        affichage.setText(calcul);
    }

    public void bouton0(View v)
    {
        calcul = calcul + "0";
        affichage.setText(calcul);
    }

    public void boutonPlus(View v)
    {
        nombres[i] = Integer.parseInt(calcul);
        calcul = "";
        affichage.setText("");
        i++;
    }

    public void boutonReset(View v)
    {
        calcul = "";
        affichage.setText(calcul);
    }

    public void boutonEgale(View v)
    {
        int result = 0;
        nombres[i] = Integer.parseInt(calcul);
        calcul = "";
        for(int j=0;j<i;j++)
        {
            result = result + nombres[j];
        }
        affichage.setText(Integer.toString(result));
    }
}
