package com.example.td11;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText Nombre;
    private int resultat;
    private int nb;
    private int reset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Nombre = (EditText) findViewById(R.id.Nombre);
        nb=0;
        reset = 0;
        resultat = 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void Addition(View v)
    {
        if(reset == 0)
        {
            if(!Nombre.getText().toString().isEmpty())
            {
                nb = Integer.parseInt(Nombre.getText().toString());
                resultat = resultat + nb;
                Nombre.setText(Integer.toString(resultat));
                reset = 1;
            }
        }
        else
        {
            if(!Nombre.getText().toString().isEmpty())
            {
                resultat = resultat + nb;
                Nombre.setText(Integer.toString(resultat));
            }
        }
    }

    public void egale(View v)
    {
        Nombre.setText(Integer.toString(resultat));
        nb=0;
        reset = 0;
    }

    public void reset(View v)
    {
        nb = 0;
        Nombre.setText("");
        resultat = 0;
        reset = 0;
    }
}
