package com.example.td1;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText nombre1;
    private EditText nombre2;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        nombre1 = (EditText)findViewById(R.id.idNombre1);
        nombre2 = (EditText)findViewById(R.id.idNombre2);
        result = (TextView)findViewById(R.id.result);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void calculs(View v)
    {
        if(!nombre1.getText().toString().isEmpty() && !nombre2.getText().toString().isEmpty()) {
            int nb1 = Integer.parseInt(nombre1.getText().toString()); //récupère le premier chiffre transformé en String puis en int
            int nb2 = Integer.parseInt(nombre2.getText().toString());

            int resultAddition = nb1 + nb2;
            int resultMultiplication = nb1*nb2;
            int resultSoustraction = nb1 - nb2;
            int resultDivision;
            if(nb2 == 0)
            {
                resultDivision = 0;
            }
            else
            {
                resultDivision = nb1/nb2;
            }
            result.setText("Addition = " + Integer.toString(resultAddition)+
                    "\nMultiplication = "+ Integer.toString(resultMultiplication)+
                    "\nSoustraction = "+ Integer.toString(resultSoustraction) +
                    "\nDivision = "+Integer.toString(resultDivision));
        }
    }
}
